# FULLSTACK XANN

## FEATURE 01: Set up Project

### DOCS

```
https://docs.nestjs.com/
```

### Install CLI

```
npm i -g @nestjs/cli
```

### Create new project

```
nest new project-xann
```

### Create new project with git

```
git clone https://github.com/nestjs/typescript-starter.git project-xann
cd project
npm install
```

### Run project

```
npm run start
localhost:3000
```

---

### Create .env file

> .env

```
KEY="VALUE"
```

### Install dotenv

```
npm install dotenv
```

### Import dotenv

> src/main.ts

```
import * as dotenv from 'dotenv'
dotenv.config()
```

### Use dotenv

```
process.env.KEY
```

---

### Install typeorm and pg

```
npm install typeorm pg
```
